@extends('layouts.app')

@section('content')

    <h1>Address Book</h1>
    
    
    <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">Address Details</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Address Book Title</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->address_book_title }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Contact Person Name</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->contact_person_name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Contact Person Number</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->contact_person_number }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Address Line 1</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->address_line_1 }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Address Line 2</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->address_line_2 }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Address Line 3</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->address_line_3 }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Pincode</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->pincode }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>City</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->city }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>State</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->state }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Country</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->country }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Default From</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->default_from }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Default To</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $addressbook->default_to }}
                        </div>
                    </div>
                </div>
                </div>
            </div>
    </div>


@endsection