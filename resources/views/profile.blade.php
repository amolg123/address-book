@extends('layouts.app')

@section('content')

    <div class="row">
        
            <div class="panel panel-default">
                <div class="panel-heading">{{ $user->name . '\'s' }} Profile</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Name</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $user->name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1"><b>Email</b>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            {{ $user->email }}
                        </div>
                    </div>
                </div>
                </div>
            </div>

    </div>

@endsection
