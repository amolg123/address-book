<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = Auth::user();
        return view('profile', compact('user'));
    }
    
    /**
     * Api Details.
     *
     * @return \Illuminate\Http\Response
     */
    public function apiDetails()
    {
        $user_id = Auth::user()->id;
        return view('apidetails', compact('user_id'));
    }
}
