<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Addressbook;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session, Auth;

class AddressbookController extends Controller
{

    public function __construct()
    {
        $this->user_id = (!empty(Auth::user())) ? Auth::user()->id : '';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $addressbook = Addressbook::where('user', '=', $this->user_id)->paginate(10);

        return view('addressbook.index', compact('addressbook'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user_id = $this->user_id;
        
        return view('addressbook.create', compact('user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        
        Addressbook::create($request->all());

        Session::flash('flash_message', 'Addressbook added!');

        return redirect('addressbook');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $addressbook = Addressbook::where('user', '=', $this->user_id)->find($id);
        return (!empty($addressbook)) ? view('addressbook.show', compact('addressbook')) : redirect('addressbook');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $addressbook = Addressbook::where('user', '=', $this->user_id)->find($id);

        return (!empty($addressbook)) ? view('addressbook.edit', compact('addressbook')) : redirect('addressbook');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $addressbook = Addressbook::findOrFail($id);
        $addressbook->update($request->all());

        Session::flash('flash_message', 'Addressbook updated!');

        return redirect('addressbook');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Addressbook::destroy($id);

        Session::flash('flash_message', 'Addressbook deleted!');

        return redirect('addressbook');
    }

}
