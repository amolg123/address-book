<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('addressbooks', function(Blueprint $table) {
                $table->increments('id');
                $table->string('address_book_title');
                $table->string('contact_person_name');
                $table->string('contact_person_number');
                $table->string('address_line_1');
                $table->string('address_line_2');
                $table->string('address_line_3');
                $table->string('pincode');
                $table->string('city');
                $table->string('state');
                $table->string('country');
                $table->boolean('default_from');
                $table->boolean('default_to');
                $table->integer('user');

                $table->timestamps();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addressbooks');
    }

}
